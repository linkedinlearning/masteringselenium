package packt.selenium.chap3_7.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Ripon on 11/4/2015.
 */
public class Documentation {
    private WebDriver driver;

    public Documentation(WebDriver driver) {
        this.driver = driver;
        System.out.println(driver.getTitle().toString());
        if (!driver.getTitle().equals("Selenium - Web Browser Automation")){
            throw new WrongPageException("Incorrect page for Documentation Page");
        }
    }

    public TestDesignConsiderations navigateToTestDesignConsiderations(){
        driver.findElement(By.xpath("//*[contains(text(),'Test Design Consideration')]")).click();
        return new TestDesignConsiderations(driver);
    }
}