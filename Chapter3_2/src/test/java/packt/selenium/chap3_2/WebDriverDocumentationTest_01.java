package packt.selenium.chap3_2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

/**
 * Created by Ripon on 11/4/2015.
 */
public class WebDriverDocumentationTest_01 {

    public static void main(String args[]){
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Alcides\\Documents\\LinedInLearning\\Ex_Files_Selenium_Testing_Tools\\Final Example Code\\Chapter1\\src\\test\\resources\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        String baseUrl = "http://www.seleniumhq.org";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.get(baseUrl + "/");
        driver.findElement(By.xpath("//*[@id='menu_documentation']/a")).click();
        driver.findElement(By.linkText("Selenium WebDriver")).click();
        System.out.println(driver.getTitle());
        assertEquals("Selenium WebDriver", driver.getTitle());
        driver.quit();
    }
}